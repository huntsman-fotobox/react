`Stand 05/23`

---

# Huntsman

## Description

Just a simple photobox! Take snapshots via your webcam and browser technologies.
[link to webpage]

## Run

```bash
> nvm use
> npm run dev
```

---

## Development

### Phases
```text
...HUNTING] [AIM] [SHOOT] [CELEBRATE] [HUNTING...
```