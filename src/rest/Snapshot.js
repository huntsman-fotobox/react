import Api from "../utils/api";

export default class Snapshot {

  /**
   * save snapshot
   * @param data
   * @returns {Promise<unknown>}
   */
  static save(data) {
    return new Promise((resolve, reject) => {
      Api.post(`/snapshot`, data)
        .then(response => {
          if(response.data['success']){
            return resolve(response.data['result']);
          } else {
            return resolve({});
          }
        })
        .catch(e => {
          console.error('[Snapshot.save]', e);
          return reject(e);
        });
    })
  }

}