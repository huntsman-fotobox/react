import Api from "../utils/api";

export default class Gallery {

  static create(gallery) {
    return new Promise((resolve, reject) => {
      Api.post(`/gallery/`, gallery)
        .then(response => {
          if(response.data['success']){
            return resolve(response.data['result']);
          } else {
            return resolve([])
          }
        })
        .catch(e => {
          console.error('[Gallery.list]', e);
          return reject(e);
        })
    });
  }

  /**
   * get list of galleries
   * @returns {Promise<unknown>}
   */
  static list() {
    return new Promise((resolve, reject) => {
      Api.get(`/gallery/list`)
        .then(response => {
          if(response.data['success']){
            return resolve(response.data['result']);
          } else {
            return resolve([])
          }
        })
        .catch(e => {
          console.error('[Gallery.list]', e);
          return reject(e);
        })
    });
  };


  /**
   * respond last ${limit} screenshots for ${id}
   * @param id
   * @param limit
   * @returns {Promise<unknown>}
   */
  static history(id, limit = 8) {
    return new Promise((resolve, reject) => {
      Api.get(`/gallery/${id}/${limit}`)
        .then(response => {
          if(response.data['success']){
            return resolve(response.data['result']);
          } else {
            return resolve([])
          }
        })
        .catch(e => {
          console.error('[Gallery.history]', e);
          return reject(e);
        })
    });
  }

}