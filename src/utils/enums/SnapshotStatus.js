export const SnapshotStatus = {
  HUNTING: "HUNTING",
  AIM: "AIM",
  SHOOT: "SHOOT",
  CELEBRATE: "CELEBRATE"
}