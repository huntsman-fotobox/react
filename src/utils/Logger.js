export default class Logger {

  /**
   * show highlighted log in console
   * eg Logger.info('name', 'a message', {some: 'data'}, '#f00');
   *
   * @param name
   * @param message
   * @param data
   * @param color
   */
  static info(name, message, data, color = "#93bd17") {
    console.info(`%c [${name}] ${message}`, `background: ${color}; color: #f3f3f3`, data);
  }

  static error(name, message, data, color = "#f40308") {
    console.error(`%c [${name}] ${message}`, `background: ${color}; color: #f3f3f3`, data);
  }
}
