import { createSlice } from "@reduxjs/toolkit";
import Logger from "../utils/Logger";

export const snapshotStore = createSlice({
  name: "snapshot",
  initialState: {
    type: "HUNTING",
    gallery: null,
    prey: null // die beute
  },
  reducers: {
    initSnapshotSession: (state, action) => {
      state.gallery = action.payload;
    },

    setStatus: (state, action) => {
      state.type = action.payload;
      Logger.info("snapshotStore", "hunting", state.type, "#333");
    },

    setPrey: (state, action) => {
      state.prey = action.payload;
      Logger.info("snapshotStore", "prey", state.prey, "#606060");
    }
  }
});

export const {
  setStatus,
  initSnapshotSession,
  setPrey
} = snapshotStore.actions;

export default snapshotStore.reducer;