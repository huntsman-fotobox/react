import { configureStore } from '@reduxjs/toolkit'

import snapshotStore from "./snapshotStore"

export const store = configureStore({
    reducer: {
        snapshotStore
    },
})
