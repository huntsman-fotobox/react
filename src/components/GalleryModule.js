import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import moment from "moment";
import Gallery from "./../rest/Gallery";
import { CreateGallery } from "./atoms/CreateGallery";
import { initSnapshotSession } from "./../redux/snapshotStore";

import { ReactComponent as ZombieSVG } from "./../images/noun-zombie-5367411.svg";
import { ReactComponent as SnapshotSVG } from "./../images/noun-add-3585716.svg";
import { ReactComponent as GallerySVG } from "./../images/noun-gallery-3585714.svg";

const GalleryModule = () => {
  const dispatch = useDispatch();

  const [promise, setPromise] = useState(true);
  const [galleries, setGalleries] = useState([]);

  useEffect(() => {
    setPromise(current => !current);

    Gallery.list()
      .then(g => {
        setGalleries(g);
        setPromise(current => !current);
      });
  }, []);


  /**
   * Choose existing gallery
   * @param event
   * @param id
   */
  function chooseGallery(event, id) {
    dispatch(initSnapshotSession(id));
  }

  return (
    <div id="gallery" className="bg-neutral-50">

      <section className="relative mx-auto h-screen w-3/4 pt-12 text-center border-b">
        <ZombieSVG className="relative mx-auto w-1/2 h-auto text-white" />
        <div className="ml-3 text-7xl font-bold">HUNTSMAN</div>

        <div className="absolute bottom-9 w-full text-center">scroll down!</div>
      </section>

      <section className="relative mx-auto w-3/4 py-9 ">
        <h2 className="mb-5 mt-0 text-2xl font-medium leading-tight text-primary">
          Add new gallery
        </h2>
        <CreateGallery />
      </section>

      <section className="relative mx-auto w-3/4 pt-9 pb-28">
        <h2 className="mb-5 mt-0 text-2xl font-medium leading-tight text-primary">
          Choose existing gallery
        </h2>

        <table className="min-w-full text-left text-sm font-light">
          <thead className="border-b font-medium bg-neutral-100 dark:border-neutral-500">
          <tr>
            <th className="px-6 py-4 rounded-tl-xl">Name</th>
            <th className="px-6 py-4">Amount Snapshots</th>
            <th className="px-6 py-4">Created at</th>
            <th className="px-6 py-4 rounded-tr-xl"></th>
          </tr>
          </thead>

          {(galleries && promise) ? (
            <tbody>
            {galleries.map((row, index) => (
              <tr key={row._id} data-id={row._id} className="border-b dark:border-neutral-500 bg-white">
                <td className="whitespace-nowrap px-6 py-4 font-bold">
                  {row.name}
                </td>
                <td className="whitespace-nowrap px-6 py-4 text-center">?</td>
                <td className="whitespace-nowrap px-6 py-4">{moment(row.created_at).format("DD.MM.YYYY")}</td>
                <td className="whitespace-nowrap px-6 py-4 flex items-center justify-end">
                  <button disabled={true} className="cursor-no-drop disabled:opacity-70">
                    <GallerySVG className="h-12 w-12 fill-neutral-500 hover:fill-neutral-900 transition" />
                  </button>

                  <button className="cursor-pointer" onClick={e => chooseGallery(e, row._id)}>
                    <SnapshotSVG className="h-12 w-12 fill-huntsman" />
                  </button>
                </td>
              </tr>
            ))}
            </tbody>
          ) : null}

        </table>
      </section>
    </div>
  );
};

export default GalleryModule;