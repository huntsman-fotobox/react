import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setStatus } from "./../redux/snapshotStore";
import { SnapshotStatus } from "./../utils/enums/SnapshotStatus";
import CameraStream from "./atoms/CameraStream";
import ScreenshotHistory from "./Screenshot/History";
import SnapshotCounter from "./Snapshot/Counter";
import ScreenshotSingle from "./Screenshot/Single";


const SnapshotModule = () => {
  const dispatch = useDispatch();
  const status = useSelector((state) => state.snapshotStore.type);

  const target = useRef();

  const KEY_SHOOT = 32;
  const KEY_FILTER = 70;

  useEffect(() => {
    document.addEventListener("keydown", aimTarget, true);

    return () => {
      document.removeEventListener("keydown", aimTarget, true);
    };
  }, [status]);


  const aimTarget = async (event) => {
    if (event.keyCode === KEY_SHOOT) {

      if (status === "HUNTING") {
        dispatch(setStatus(SnapshotStatus.AIM));
      }
    }

    if (event.keyCode === KEY_FILTER) {
      target.current.filter();
    }
  };

  return (
    <div id="snapshot" className="relative z-0 overflow-hidden">
      <ScreenshotHistory />

      <ScreenshotSingle />
      <SnapshotCounter duration={parseInt(process.env.REACT_APP_DURATION_AIM)} />
      <CameraStream />
    </div>
  );
};

export default SnapshotModule;