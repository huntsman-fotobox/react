import React from "react";
import { connect } from "react-redux";
import { setStatus } from "../../redux/snapshotStore";

class ScreenshotSingle extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const UPLOAD_DIR = `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_UPLOADS_FOLDER}`;

    if (this.props.snapshotStore.prey) {
      const prey = this.props.snapshotStore.prey;

      return (
        <div className="absolute t-0 l-0 z-50 w-full h-full flex items-center bg-black">
          <img className="min-w-full" src={`${UPLOAD_DIR}${prey.path}/${prey.file_name}`} alt="Shooted!" />
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  snapshotStore: state.snapshotStore
});

export default connect(mapStateToProps, {
  setStatus
})(ScreenshotSingle);