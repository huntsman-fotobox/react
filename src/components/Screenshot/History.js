import { useEffect, useState } from "react";
import Gallery from "../../rest/Gallery";
import { useSelector } from "react-redux";
import moment from "moment/moment";
import { SnapshotStatus } from "../../utils/enums/SnapshotStatus";

const ScreenshotHistory = () => {
  const [promise, setPromise] = useState(true);
  const [history, setHistory] = useState({ snapshots: [], gallery: null });
  const snapshotSession = useSelector((state) => state.snapshotStore.gallery);
  const snapshotStatus = useSelector((state) => state.snapshotStore.type);

  const UPLOAD_DIR = `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_UPLOADS_FOLDER}`;

  useEffect(() => {
    if (snapshotStatus === SnapshotStatus.HUNTING) {
      setPromise(false);

      Gallery.history(snapshotSession).then(history => {
        setHistory({
          snapshots: history.snapshots,
          gallery: history.gallery
        });
        setPromise(true);
      });
    }
  }, [snapshotStatus]);

  return (
    <>
      {(history.gallery) && (
        <section id="history"
                 className={`${(snapshotStatus !== SnapshotStatus.HUNTING) && '-hide'} absolute bottom-0 left-0 w-full z-30 pb-2 pt-9 bg-gradient-to-t from-black to-transparent`}>
          <div className="relative mx-auto w-11/12">
            <div className="flex items-center justify-start">
              {history.snapshots.map((s, index) => (
                <div className="image rounded-md overflow-hidden mx-1"
                     key={s._id}>
                  <img className="max-h-20" src={`${UPLOAD_DIR}${s.path}/${s.file_name}`} alt="" />
                </div>
              ))}
            </div>
            <h3 className="mt-1 text-xs text-white opacity-50">
              {moment(history.gallery.created_at).format("DD.MM.YYYY")}
              <span className="mx-1">-</span>
              {history.gallery.name}
            </h3>
          </div>
        </section>
      )}
    </>
  );
};

export default ScreenshotHistory;