import React, { forwardRef, useCallback, useEffect, useImperativeHandle, useRef, useState } from "react";
import Webcam from "react-webcam";
import { useDispatch, useSelector } from "react-redux";
import Base64Utils from "./../../utils/Base64Utils";
import Logger from "../../utils/Logger";
import Snapshot from "./../../rest/Snapshot";
import { setPrey, setStatus } from "../../redux/snapshotStore";
import { Spinner } from "./Spinner";
import { SnapshotStatus } from "../../utils/enums/SnapshotStatus";

/**
 * https://www.npmjs.com/package/react-webcam
 * @type {React.ForwardRefExoticComponent<React.PropsWithoutRef<{}> & React.RefAttributes<unknown>>}
 */
const CameraStream = forwardRef((props, ref) => {
  const dispatch = useDispatch();

  const filterSize = 5;
  const [filter, setFilter] = useState(0);

  const webcamRef = useRef(null);
  const snapshotSession = useSelector((state) => state.snapshotStore.gallery);
  const snapshotStatus = useSelector((state) => state.snapshotStore.type);
  const videoConstraints = {
    width: 800,
    height: 600,
    aspectRatio: 0.6666666667
  };

  useEffect(() => {
    if(snapshotStatus === SnapshotStatus.SHOOT){
      takeScreenshot()
    }
  }, [snapshotStatus]);


  useImperativeHandle(ref, () => ({
    filter() {
      setFilter(current => (current !== filterSize) ? current + 1 : 0);
      Logger.info("SNAPSHOT", "filter", filter);
    }
  }));

  const takeScreenshot = useCallback(() => {
    const stream = webcamRef.current.getScreenshot();

    // Split the base64 string in data and contentType
    let block = stream.split(";");
    let contentType = block[0].split(":")[1];
    let realData = block[1].split(",")[1];

    // Convert it to a blob to upload
    let blob = Base64Utils.b64toBlob(realData, contentType);
    let formDataToUpload = new FormData();

    formDataToUpload.append("snapshot", blob);
    formDataToUpload.append("gallery", snapshotSession);
    formDataToUpload.append("filter", null);

    Snapshot.save(formDataToUpload)
      .then(s => {
        dispatch(setPrey(s));
        dispatch(setStatus(SnapshotStatus.CELEBRATE));

        setTimeout(() => {
          dispatch(setPrey(null));
          dispatch(setStatus(SnapshotStatus.HUNTING));
        }, parseInt(process.env.REACT_APP_DURATION_CELEBRATE));
      });
  }, [webcamRef, snapshotSession]);

  const styles = {};

  return (
    <section id="camera-stream" className="-four-square relative w-full h-screen flex items-center justify-center">
      <Webcam
        className="flex-1 relative z-10"
        style={styles}
        data-filter={filter}
        imageSmoothing={true}
        mirrored={true}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        videoConstraints={videoConstraints}
        audio={false}
      />

      <Spinner show={true} />
    </section>
  );
});

export default CameraStream;