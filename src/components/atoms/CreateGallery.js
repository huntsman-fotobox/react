import React, { useState } from "react";
import Gallery from "../../rest/Gallery";
import { initSnapshotSession } from "../../redux/snapshotStore";
import { useDispatch } from "react-redux";
import Logger from "../../utils/Logger";

import { ReactComponent as AddSVG } from "./../../images/noun-add-3585716.svg";


export function CreateGallery(props) {
  const dispatch = useDispatch();
  const [gallery, setGallery] = useState({
    name: ""
  });

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    setGallery({
      ...gallery,
      [name]: value
    });
  };

  const create = (event) => {
    event.preventDefault();

    Gallery.create(gallery)
      .then(response => {
        Logger.info("GALLERY", "created", response);
        dispatch(initSnapshotSession(response._id));
      });
  };

  return (
    <form action="" className="relative w-full flex items-center">
      <input type="text"
             className="h-12 px-6 mr-6 w-1/2 border rounded-xl font-medium bg-white"
             name="name"
             value={gallery.name}
             onChange={handleChange} />
      <button className="cursor-pointer" onClick={create}>
        <AddSVG className="h-12 w-12" />
      </button>
    </form>
  );
}