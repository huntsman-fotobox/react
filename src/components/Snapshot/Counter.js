import React from "react";
import { connect } from "react-redux";
import Countdown from "react-countdown";
import { setStatus } from "./../../redux/snapshotStore";
import { SnapshotStatus } from "./../../utils/enums/SnapshotStatus";

class SnapshotCounter extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const defaultRenderer = ({ seconds, completed }) => {
      if (completed) {
        this.props.setStatus(SnapshotStatus.SHOOT);
      }

      return (<div data-seconds={seconds}>{seconds}</div>);
    }

    if (this.props.snapshotStore.type === SnapshotStatus.AIM) {
      const duration = this.props.duration || 3000;
      return (
        <div className="absolute top-0 left-0 w-full h-screen flex items-center justify-center z-30">
          <div id="countdown" className="text-9xl font-medium leading-tight text-primary text-white">
            <Countdown
              date={Date.now() + duration}
              renderer={defaultRenderer}
            />
          </div>
        </div>
      );
    }
  }

}

const mapStateToProps = state => ({
  snapshotStore: state.snapshotStore
});

export default connect(mapStateToProps, {
  setStatus
})(SnapshotCounter);