import React from 'react'
import App from "./App";

export const Root = () => {

    // Page middleware
    // e.g. location.history

    return (
        <App />
    )
}