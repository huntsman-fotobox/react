import {useSelector} from "react-redux";
import GalleryModule from "./components/GalleryModule";
import SnapshotModule from "./components/SnapshotModule";

function App() {
  const snapshotSession = useSelector((state) => state.snapshotStore.gallery);

  return (
    <main id="huntsman" className="relative min-h-screen bg-neutral-900">
      {(snapshotSession) ? (
        <SnapshotModule />
      ) : (
      <GalleryModule />
      )}
    </main>
  );
}

export default App;
