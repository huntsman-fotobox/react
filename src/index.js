import React from "react";
import { createRoot } from "react-dom/client";
import { HashRouter } from "react-router-dom";
import { Helmet } from "react-helmet";

import { Provider } from "react-redux";
import { store } from "./redux/store";
import { Root } from "./Root";
import pckgJson from "./../package.json";

import "./index.scss";

const container = document.getElementById("root");
const root = createRoot(container); // createRoot(container!) if you use TypeScript

root.render(
  <HashRouter>
    <Helmet>
      <title>Huntsman | official</title>
      <meta property="version" content={pckgJson.version} />
      <meta name="timestamp" content={new Date()} />
      <meta property="og:type" content="website" />
    </Helmet>

    <Provider store={store}>
      <Root />
    </Provider>

  </HashRouter>
);
